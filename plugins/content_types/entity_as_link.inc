<?php

/**
 * @file =============================================================================
 * Ref.: http://drupal.stackexchange.com/questions/33336/how-does-one-create-a-new-ctools-plugin-content-type-access-etc
 * ========================================================================== .*/

$plugin = array(
  'title' => t('Entity as Link'),
  'description' => t('Output a selected Entity as a link to that Entity.'),
  'category' => t('ADAPL'),
  'admin title' => 'entity_as_link_content_type_admin_title',
  'admin info' => 'entity_as_link_content_type_admin_info',
  'single' => TRUE,
  'icon' => 'icon_node.png',
  'js' => array('misc/autocomplete.js'),
  'css' => array(
    drupal_get_path('theme', 'rubik') . '/core.css',
    drupal_get_path('theme', 'aps') . '/css/core-mod.css',
  ),
  'edit form' => array(
    'entity_as_link_content_type_edit_form' => t('Configure Entity as Link Settings'),
  ),
  'render callback' => 'entity_as_link_content_type_render',
  'top level' => TRUE,
  'defaults' => array(
    'field_settings' => array(
      'entity_list' => _entity_as_link_content_type_helper_get_entity_list(array('return_values' => array('list'))),
      'settings_field_list' => array('entity', 'bundle', 'eid', 'custom_text'),
    ),
  ),
);

/**
 * The form to add or edit a node as content.
 */
function entity_as_link_content_type_edit_form($form, &$form_state) {
  // Get conf.
  $conf = $form_state['conf'];
  // Get values.
  if (isset($form_state['conf']['values'])) {
    $values = $form_state['conf']['values'];
    unset($form_state['conf']['values']);
  }
  else {
    $values = _entity_as_link_content_type_get_edit_form_values($form_state);
  }

  $form['#tree'] = TRUE;

  // Fieldset to hold settings fields that need to be updated according to $entity.
  $form['entity_subfields'] = array(
    '#type' => 'fieldset',
    '#title' => 'Entity Settings',
    '#weight' => 21,
    '#prefix' => '<div id="entity_subfields">',
    '#suffix' => '</div>',
  );

  // Field generating funcs follow a set naming convention - "_entity_as_link_content_type_get_{$field_name}_field"
  // confirm that the func exists and then call it to add it to the $form fieldset
  // if the field needs to be nested - e.g. inside $form['entity_subfields'] - then use a list of nested fields to run a check and perform the nesting as in $entity_settings_fields.
  $entity_settings_fields = array('bundle', 'eid');
  foreach ($conf['field_settings']['settings_field_list'] as $field_name) {
    $func_name = "_entity_as_link_content_type_get_{$field_name}_field";
    if (is_callable($func_name)) {
      if (in_array($field_name, $entity_settings_fields)) {
        $form['entity_subfields'][$field_name] = call_user_func($func_name, $values['entity'], $values, $form_state);
      }
      else {
        $form[$field_name] = call_user_func($func_name, $values['entity'], $values, $form_state);
      }
    }
  }

  // Remove undesired ctools native content type form elements.
  $unwanted_fields = array(
    'override_title',
    'override_title_text',
    'override_title_markup',
    'aligner_start',
    'aligner_stop',
  );
  foreach ($unwanted_fields as $field_name) {
    unset($form[$field_name]);
  }

  return $form;
}

/**
 * Ajax callback handler.
 */
function entity_as_link_content_type_edit_form_ajax_callback($js = NULL) {
  // Include ctools libraries.
  ctools_include('ajax');

  // Pulls form from cache and updates $form_state with input from $_POST.
  list($form, $form_state, $form_id, $form_build_id) = ajax_get_form();

  // Rebuilds the form.
  drupal_process_form($form_id, $form, $form_state);

  // Get the triggering element's field name.
  $trigger = $form_state['triggering_element']['#name'];

  // Place holder for the return value.
  $ajax_return_element = NULL;

  // Change the form element that is returned based upon the trigger.
  switch ($trigger) {
    // ============================================================.
    case 'entity':
      $ajax_return_element = $form['entity_subfields'];
      break;

    // ============================================================.
    case 'entity_subfields[bundle]':
      $ajax_return_element = $form['entity_subfields']['eid'];
      break;

    // ============================================================.
    default:
      break;
  }

  // Initiate response.
  ajax_deliver($ajax_return_element);
}

/**
 * Pull values from $form_state['values'] OR $form_state['input'] into $form_state['conf'].
 */
function entity_as_link_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['values'] = _entity_as_link_content_type_get_edit_form_values($form_state);
}

/**
 * Ensure that an Image or Media field has been selected.
 */
function entity_as_link_content_type_edit_form_validate($form, &$form_state) {
  // Convenience assignments.
  $val = $form_state['values'];
  $entity_type = $val['entity'];
  $bundle = $val['entity_subfields']['bundle'];
  $bundles_info = field_info_instances($entity_type, $bundle);
  $eid = entity_autocomplete_get_id($val['entity_subfields']['eid']);

  // Ensure correct value exists for Entity.
  $entity_info = entity_get_info();
  $entity_list = array_keys($entity_info);
  if (!in_array($entity_type, $entity_list)) {
    form_set_error('entity', t('The specified Entity does not exist.'));

    return FALSE;
  }

  // Ensure the EID is retrievable.
  $loaded_entities = entity_load($entity_type, array($eid));
  if (!$loaded_entities || (is_array($loaded_entities) && empty($loaded_entities))) {
    form_set_error('entity_subfields][eid', t('There was a problem with the Entity ID specified. The selected Entity does not exist.'));

    return FALSE;
  }
  else {
    $entity = array_shift($loaded_entities);
  }

  // Different types of entities currently use different key names to represent the bundle,
  // check against entity type and specify the entity type and specify value to check $bundle against.
  $bundle_key_name = NULL;
  switch ($entity_type) {
    // ============================================================.
    case 'taxonomy_term':
      $bundle_key_name = 'vocabulary_machine_name';
      break;

    // ============================================================.
    case 'user':
    case 'taxonomy_vocabulary':
      // These entity types are only declared with a single bundle type
      // equivalent to their machine names so we skip the bundle type check.
      $bundle_key_name = FALSE;
      break;

    // ============================================================.
    default:
      $bundle_key_name = 'type';
      break;
  }
  // Ensure the Entity is of the correct bundle type.
  if (is_string($bundle_key_name)) {
    if (isset($entity) && $entity->{$bundle_key_name} !== $bundle) {
      form_set_error('entity_subfields][eid', t('There was a problem with the Entity ID specified. The Entity was not of the selected Entity Type.'));
    }
  }

  // Validate Bundle / Entity Type.
  list($bundle_list) = _entity_as_link_content_type_helper_get_bundle_list($entity_type);
  if (!in_array($bundle, array_keys($bundle_list))) {
    form_set_error('entity_subfields][bundle', t("The selected {$entity_info[$entity_type]['label']} Type does not exist."));
  }
}

/**
 * Render Entity options chosen by user.
 */
function entity_as_link_content_type_render($subtype, $conf, $panel_args) {
  // Pull out values from configuration.
  $values = $conf['values'];

  // Get configuration values.
  $entity_type = $values['entity'];
  $bundle_type = $values['bundle'];
  $eid = entity_autocomplete_get_id($values['eid']);
  $use_custom_text = (bool) $values['use_custom_text'];
  $custom_link_text = check_plain($values['link_text']);

  $block = new stdClass();

  if (!is_numeric($eid)) {
    return FALSE;
  }

  // Try to load the entity.
  $loaded = entity_load($entity_type, array($eid));
  $entity = (!empty($loaded)) ? array_shift($loaded) : FALSE;

  // Check for entity and continue.
  if (is_object($entity)) {

    // Check permissions.
    if (!entity_access('view', $entity_type, $entity)) {
      return FALSE;
    }

    // Start building the block to return for use by page mangaer/ctools enabled module.
    $block->delta = $eid;

    // Get entity's title.
    $entity_title = check_plain($entity->title);

    // Get aliased path.
    $link_path = drupal_get_path_alias("{$entity_type}/{$eid}", $GLOBALS['language']->language);

    // Discern which title will be used as link text.
    $link_title = ($use_custom_text) ? $custom_link_text : $entity_title;

    // Create and pass generated link to block.
    $block->content = l("<span>{$link_title}</span>", $link_path, array(
      'html' => TRUE,
    ));

    return $block;
  }
  else {
    return FALSE;
  }
}

/**
 * Returns the administrative title for a node.
 */
function entity_as_link_content_type_admin_title($subtype, $conf) {
  $values = $conf['values'];
  $entity_type = $values['entity'];
  $bundle_type = $values['bundle'];
  $eid = entity_autocomplete_get_id($values['eid']);
  $retrieved_entity = entity_load($entity_type, array($eid));
  $entity = array_pop($retrieved_entity);
  $title = check_plain($entity->title);

  return t('[@entity_type ID-@eid] @title | Entity as Link', array(
    '@eid' => $eid,
    '@title' => $title,
    '@entity_type' => ucwords($entity->type),
  ));
}

/**
 * Display the administrative information for a node pane.
 */
function entity_as_link_content_type_admin_info($subtype, $conf) {
  $values = $conf['values'];

  $items = array();

  foreach ($values as $var_name => $var_value) {
    $items[] = "<strong>\${$var_name}:</strong> {$var_value}";
  }

  $block = new stdClass();

  $block->title = t('Entity as Link: Settings');

  $block->content = theme_item_list(array(
    'items' => $items,
    'title' => t('Settings'),
    'type' => 'ul',
    'attributes' => array(),
  ));

  return $block;
}

/**
 * Helper to discern from where form values should be pulled for form building.
 */
function _entity_as_link_content_type_get_edit_form_values(&$form_state) {
  // Var for keeping track of which use case this func was called in.
  $trigger_state = '';

  // Check to see at what point this func was called - could be one of three or four cases
  // 1) form requested for the very first time - meaning that we need fresh default values.
  if (!isset($form_state['input']['_triggering_element_name']) && !$form_state['submitted']) {
    $trigger_state = 'defaults';
  }
  // 2) form requested by an ajax call, this means values will need to be updated from $form_state['input'].
  elseif (isset($form_state['input']['_triggering_element_name'])) {
    $trigger_state = 'input';
  }
  // 3) form requested onSubmit or after it was submitted and values were
  // saved - values need to be pulled from $form_state['values'].
  else {
    $trigger_state = 'values';
  }

  // get/set values appropriate to triggering use case and return them.
  return _entity_as_link_content_type_set_edit_form_values($trigger_state, $form_state);
}

/**
 * Helper to actually get the values that will be used as #default_value for form fields.
 */
function _entity_as_link_content_type_set_edit_form_values($form_state_key_to_use, $form_state) {
  // Values to be used as #default_value.
  $values = array();

  // If $form_state_key_to_use == 'defaults', it means that this is the initial form build.
  if ($form_state_key_to_use == 'defaults' || $form_state_key_to_use == 'input') {
    // Pull out $conf for cleanliness.
    $conf = $form_state['conf'];

    // get-set Entity.
    if ($form_state_key_to_use == 'defaults') {
      reset($conf['field_settings']['entity_list']);
      $values['entity'] = $entity = key($conf['field_settings']['entity_list']);
    }
    else {
      $values['entity'] = $entity = $form_state[$form_state_key_to_use]['entity'];
    }

    // get-set Bundle.
    if ($form_state_key_to_use == 'defaults') {
      list(, $bundle) = _entity_as_link_content_type_helper_get_bundle_list($entity);
      $values['bundle'] = $bundle;
    }
    else {
      $values['bundle'] = $bundle = $form_state[$form_state_key_to_use]['entity_subfields']['bundle'];
    }

    // Set EID.
    $values['eid'] = '';

    // Set custom link text fieldset values.
    $values['use_custom_text'] = 0;
    $values['link_text'] = '';

    return $values;
  }
  else {
    $input = $form_state[$form_state_key_to_use];
    $entity = $values['entity'] = $input['entity'];
    $bundle = $values['bundle'] = $input['entity_subfields']['bundle'];
    $values['eid'] = $input['entity_subfields']['eid'];
    $values['use_custom_text'] = $input['custom_text']['use_custom_text'];
    $values['link_text'] = $input['custom_text']['link_text'];

    return $values;
  }
}

/**
 * Helper to generate select widget of input Entity's Bundles.
 */
function _entity_as_link_content_type_get_entity_field($entity, $values, $form_state) {
  $conf = $form_state['conf'];

  reset($conf['field_settings']['entity_list']);
  $default_value = (isset($values['entity'])) ? $values['entity'] : key($conf['field_settings']['entity_list']);

  return array(
    '#title' => t('Select Entity Type'),
    '#type' => 'select',
    '#options' => $conf['field_settings']['entity_list'],
    '#default_value' => $default_value,
    '#weight' => 20,
    '#ajax' => array(
      'path' => 'ctools_aep/%ctools_js/entity_as_link',
      'wrapper' => 'entity_subfields',
      'effect' => 'fade',
    ),
  );
}

/**
 * Helper to generate select widget of input Entity's Bundles.
 */
function _entity_as_link_content_type_get_bundle_field($entity, $values, $form_state) {
  // Generate a keyed array of bundle names.
  list($bundles, $first_bundle_name) = _entity_as_link_content_type_helper_get_bundle_list($entity);

  // Set the default value depending upon availability of the same from $defaults.
  $bundle = ((isset($values['bundle']) && strlen($values['bundle'])) && (isset($values['entity']) && ($entity == $values['entity']))) ? $values['bundle'] : $first_bundle_name;

  $trace = debug_backtrace();
  $caller = array_shift($trace);

  return array(
    '#title' => t(ucwords($entity) . ' Type'),
    '#type' => 'select',
    '#id' => "{$entity}_bundles",
    '#options' => $bundles,
    '#default_value' => $bundle,
    '#weight' => 22,
    '#ajax' => array(
      'path' => 'ctools_aep/%ctools_js/entity_as_link',
      'wrapper' => 'eid_field',
      'effect' => 'fade',
    ),
  );
}

/**
 * Helper to generate autocomplete widget for retrieving an ID based upon Entity and Bundle types.
 */
function _entity_as_link_content_type_get_eid_field($entity, $values, $form_state) {
  // Bundle by current entity check - necessary when the form has been submitted and then reopened.
  list($cur_entity_bundle_list, $cur_entity_bundle) = _entity_as_link_content_type_helper_get_bundle_list($entity);

  // Set $bundle if none was provided.
  if (isset($values['bundle']) && !empty($values['bundle']) && in_array($values['bundle'], array_keys($cur_entity_bundle_list))) {
    $bundle = $values['bundle'];
  }
  else {
    $bundle = $cur_entity_bundle;
  }

  // Clean up entity and bundle names for use in form element label.
  $ent_name = ucwords($entity);
  $bun_name = ucwords($bundle);

  // Get default value of eid.
  $default_eid = (isset($values['eid']) && strlen($values['eid']) && isset($values['bundle']) && $values['bundle'] == $bundle) ? $values['eid'] : '';

  // ref: http://drupal.org/project/entity_autocomplete
  return array(
    '#title' => t("Enter the title or ID of a {$bun_name} {$ent_name}."),
    '#type' => 'textfield',
    '#id' => "{$entity}_{$bundle}_eid_input",
    '#default_value' => $default_eid,
    '#maxlength' => 512,
    '#autocomplete_path' => "entity-autocomplete/bundle/{$entity}/{$bundle}",
    '#weight' => 24,
    '#prefix' => "<div id='eid_field'>",
    '#suffix' => '</div>',
  );
}

/**
 * Helper to generate autocomplete widget for retrieving an ID based upon Entity and Bundle types.
 */
function _entity_as_link_content_type_get_custom_text_field($entity, $values, $form_state) {
  // Get default values.
  $default_checkbox_value = isset($values['use_custom_text']) ? $values['use_custom_text'] : 0;
  $default_link_text_value = isset($values['link_text']) ? $values['link_text'] : '';

  // Setup fieldset form element.
  $fieldset = array(
    '#type' => 'fieldset',
    '#title' => 'Custom Link Text Settings',
    '#weight' => 30,
  );
  $fieldset['use_custom_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use custom link text.'),
    '#description' => t('Please check if you wish to customise the link text. If left unchecked, the Entity\'s title will be used as the link text.'),
    '#default_value' => $default_checkbox_value,
  );
  $fieldset['link_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Link Text'),
    '#description' => t('Please enter the custom text you would like the link to display.'),
    '#default_value' => $default_link_text_value,
    '#states' => array(
      'visible' => array(
        'input[type="checkbox"][name*="use_custom_text"]' => array('checked' => TRUE),
      ),
      'disabled' => array(
        'input[type="checkbox"][name*="use_custom_text"]' => array('checked' => FALSE),
      ),
    ),
  );

  return $fieldset;
}

/**
 * Helper to generate select widget of input Entity's Bundles.
 *
 * @param bool $options
 *   ['full_info'] Whether to directly return the original value of entity_get_info()
 *
 * @param array $options
 *   ['return_values'] Array of options to indicate which values to return.
 *
 * @param string $options
 *   ['return_values']['list'] Return ONLY the list of entities.
 *
 * @param string $options
 *   ['return_values']['default'] Return ONLY the default entity.
 *
 * @return array array($entity_list, $default_entity)
 */
function _entity_as_link_content_type_helper_get_entity_list($options = array()) {
  $opts = array_merge(array(
    'full_info' => FALSE,
    'return_values' => array(
      'list',
      'default',
    ),
  ), $options);

  // Get the list of bundles for this Entity.
  $entity_info = entity_get_info();

  if ($opts['full_info']) {
    return $entity_info;
  }

  // Holder for select element's options.
  $entities = array();

  // Ignored entity types - these are entities that are either incompatible with entity_autocomplete module or are irrelevant.
  $ignored_entities = array('wysiwyg_profile', 'field_collection_item');

  // Build an array for use as the select element's options.
  foreach ($entity_info as $key => $val) {
    if (!in_array($key, $ignored_entities)) {
      $entities[$key] = $val['label'];
    }
  }

  // Holder for the first bundle name to use as default value if $defaults['bundle'] is empty.
  reset($entities);
  $default_entity = (string) key($entities);

  // Decide what needs to be returned and do so.
  if (in_array('list', $opts['return_values']) && in_array('default', $opts['return_values'])) {
    return array($entities, $default_entity);
  }
  elseif (in_array('list', $opts['return_values'])) {
    return $entities;
  }
  elseif (in_array('default', $opts['return_values'])) {
    return $default_entity;
  }
  else {
    return array($entities, $default_entity);
  }
}

/**
 * Helper to generate select widget of input Entity's Bundles.
 */
function _entity_as_link_content_type_helper_get_bundle_list($entity) {
  // Get the list of bundles for this Entity.
  $bundles_info = field_info_bundles($entity);

  // Holder for select element's options.
  $bundles = array();

  // Build an array for use as the select element's options.
  foreach ($bundles_info as $key => $val) {
    $bundles[$key] = $val['label'];
  }

  // Holder for the first bundle name to use as default value if $defaults['bundle'] is empty.
  reset($bundles);
  $default_bundle = (string) key($bundles);

  return array($bundles, $default_bundle);
}

/**
 * A helpful note on states for this form.
 *
 * When the form is first opened by choosing the content type for a new Pane, the following is true:
 *    !isset($form_state['input']['_triggering_element_name']) && !$form_state['submitted']
 *
 * When an ajax call has been made to change some part of the form, the following will be true:
 *    isset($form_state['input']['_triggering_element_name'])
 *
 * Finally, in the last use-case scenario, when the form has been submitted and
 * is re-opened from the Pane management page, the following will hold true:
 *    isset($form_state['values']) && isset($form_state['conf']['values])
 *        - please note that $form_state['conf']['values] is set in
 *            entity_as_link_content_type_edit_form_submit()
 *          and then removed again at the beginning of
 *            entity_as_link_content_type_edit_form()
 */
