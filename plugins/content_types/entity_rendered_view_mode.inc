<?php

/**
 * @file =============================================================================
 * TODO!!!
 * todo: form validation
 * todo: content type render function
 * ========================================================================== .*/

/* =============================================================================
    Ref.: http://drupal.stackexchange.com/questions/33336/how-does-one-create-a-new-ctools-plugin-content-type-access-etc
   ========================================================================== */
$plugin = array(
  'title' => t('Rendered Entity'),
  'description' => t('Output a selected Entity . Requires an appropriate Entity such as a Node or User as the content source of the Pane as well as an Image/Media field on the Entity.'),
  'category' => t('ADAPL'),
  'admin title' => 'entity_rendered_view_mode_content_type_admin_title',
  'admin info' => 'entity_rendered_view_mode_content_type_admin_info',
  'single' => TRUE,
  'icon' => 'icon_node.png',
  'js' => array('misc/autocomplete.js'),
  'css' => array(
    drupal_get_path('theme', 'rubik') . '/core.css',
    drupal_get_path('theme', 'aps') . '/css/core-mod.css',
  ),
  'edit form' => array(
    'entity_rendered_view_mode_content_type_edit_form' => t('Configure Rendered Entity Settings'),
  ),
  'render callback' => 'entity_rendered_view_mode_content_type_render',
  'top level' => TRUE,
  'defaults' => array(
    'field_settings' => array(
      'entity_list' => array(
        'node' => 'Node',
        'file' => 'File',
        'user' => 'User',
      ),
      'settings_field_list' => array('entity', 'bundle', 'view_mode', 'eid'),
      'ignored_view_modes' => array(
        'rss',
        'search_index',
        'search_result',
        'diff_standard',
        'entityreference_view_widget',
        'token',
        'empty',
      ),
    ),
  ),
);

/**
 * The form to add or edit a node as content.
 */
function entity_rendered_view_mode_content_type_edit_form($form, &$form_state) {
  // Get conf.
  $conf = $form_state['conf'];
  // Get values.
  if (isset($form_state['conf']['values'])) {
    $values = $form_state['conf']['values'];
    unset($form_state['conf']['values']);
  }
  else {
    $values = _entity_rendered_view_mode_content_type_get_edit_form_values($form_state);
  }

  $form['#tree'] = TRUE;

  // Fieldset to hold settings fields that need to be updated according to $entity.
  $form['entity_subfields'] = array(
    '#type' => 'fieldset',
    '#title' => 'Entity Settings',
    '#weight' => 21,
    '#prefix' => '<div id="entity_subfields">',
    '#suffix' => '</div>',
  );

  // Field generating funcs follow a set naming convention - "_entity_rendered_view_mode_content_type_get_{$field_name}_field"
  // confirm that the func exists and then call it to add it to the $form fieldset
  // if the field needs to be nested - e.g. inside $form['entity_subfields'] - then use a list of nested fields to run a check and perform the nesting as in $entity_settings_fields.
  $entity_settings_fields = array('bundle', 'view_mode', 'eid');
  foreach ($conf['field_settings']['settings_field_list'] as $field_name) {
    $func_name = "_entity_rendered_view_mode_content_type_get_{$field_name}_field";
    if (is_callable($func_name)) {
      if (in_array($field_name, $entity_settings_fields)) {
        $form['entity_subfields'][$field_name] = call_user_func($func_name, $values['entity'], $values, $form_state);
      }
      else {
        $form[$field_name] = call_user_func($func_name, $values['entity'], $values, $form_state);
      }
    }
  }

  // Remove undesired ctools native content type form elements.
  $unwanted_fields = array(
    'override_title',
    'override_title_text',
    'override_title_markup',
    'aligner_start',
    'aligner_stop',
  );
  foreach ($unwanted_fields as $field_name) {
    unset($form[$field_name]);
  }

  return $form;
}

/**
 * Ajax callback handler.
 */
function entity_rendered_view_mode_content_type_edit_form_ajax_callback($js = NULL) {
  // Include ctools libraries.
  ctools_include('ajax');

  // Pulls form from cache and updates $form_state with input from $_POST.
  list($form, $form_state, $form_id, $form_build_id) = ajax_get_form();

  // Rebuilds the form.
  drupal_process_form($form_id, $form, $form_state);

  // Get the triggering element's field name.
  $trigger = $form_state['triggering_element']['#name'];

  // Place holder for the return value.
  $ajax_return_element = NULL;

  // Change the form element that is returned based upon the trigger.
  switch ($trigger) {
    // ============================================================.
    case 'entity':
      $ajax_return_element = $form['entity_subfields'];
      break;

    // ============================================================.
    case 'entity_subfields[bundle]':
      $ajax_return_element = $form['entity_subfields']['eid'];
      break;

    // ============================================================.
    default:
      break;
  }

  // Initiate response.
  ajax_deliver($ajax_return_element);
}

/**
 * Pull values from $form_state['values'] OR $form_state['input'] into $form_state['conf'].
 */
function entity_rendered_view_mode_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['values'] = _entity_rendered_view_mode_content_type_get_edit_form_values($form_state);
}

/**
 * Ensure that an Image or Media field has been selected.
 */
function entity_rendered_view_mode_content_type_edit_form_validate($form, &$form_state) {
  // Convenience assignments.
  $val = $form_state['values'];
  $entity_value = $val['entity'];
  $bundle = $val['entity_subfields']['bundle'];
  $bundles_info = field_info_instances($entity_value, $bundle);
  $eid = entity_autocomplete_get_id($val['entity_subfields']['eid']);

  // Ensure correct value exists for Entity.
  $entity_info = entity_get_info();
  $entity_list = array_keys($entity_info);
  if (!in_array($entity_value, $entity_list)) {
    form_set_error('entity', t('The specified Entity does not exist.'));

    return FALSE;
  }

  // Ensure the EID is retrievable.
  $entities_loaded = entity_load($entity_value, array($eid));
  if (!$entities_loaded || (is_array($entities_loaded) && empty($entities_loaded)) || !is_object($entities_loaded[$eid])) {
    form_set_error('entity_subfields][eid', t('There was a problem with the Entity ID specified. The selected Entity does not exist.'));

    return FALSE;
  }
  else {
    $entity = $entities_loaded[$eid];
  }

  // Ensure the Entity is of the correct bundle type.
  if (isset($entity) && $bundle !== 'user' && isset($entity->type) && $entity->type !== $bundle) {
    form_set_error('entity_subfields][eid', t('There was a problem with the Entity ID specified. The Entity was not of the selected Entity Type.'));
  }

  // Validate Bundle / Entity Type.
  list($bundle_list) = _entity_rendered_view_mode_content_type_helper_get_bundle_list($entity_value);
  if (!in_array($bundle, array_keys($bundle_list))) {
    form_set_error('entity_subfields][bundle', t("The selected {$entity_info[$entity_value]['label']} Type does not exist."));
  }
}

/**
 * Render Entity options chosen by user.
 */
function entity_rendered_view_mode_content_type_render($subtype, $conf, $panel_args) {
  // Pull out the values provided by user input onSubmit.
  $entity = $bundle = $view_mode = $eid = NULL;
  $values = $conf['values'];
  extract($values);

  // Free up $entity for the actually loaded entity.
  $entity_type = $entity;

  // Get configuration values.
  $eid = _ctools_aep_entity_autocomplete_get_id($eid);

  // Initialise $block for return.
  $block = new stdClass();

  // Support for entity translation not native to Drupal 7 Core, need i18n module for that.
  // Leaving this here as a reminder that translation is not active. Not working on it for lack of time.
  // @todo - Need to incorporate entity translation via i18n module.
  /*if(module_exists('translation')) {
  if($translations = module_invoke('translation', 'node_get_translations', $nid)) {
  if(isset($translations[$GLOBALS['language']->language])) {
  $nid = $translations[$GLOBALS['language']->language]->nid;
  }
  }
  }*/

  if (!is_numeric($eid)) {
    return FALSE;
  }

  // entity_load() can load multiple entities in one go.
  // @todo - Need to make this module capable of handling multiple EIDs, for now, is hardcoded to use the first on the stack
  $entities_loaded = entity_load($entity_type, array($eid));

  $entity = $entities_loaded[$eid];

  if (!entity_access('view', $entity_type, $entity)) {
    return FALSE;
  }

  // Don't store viewed node data on the node, this can mess up other views of the node.
  $entity = clone($entity);
  $block->delta = $eid;
  $entity_title = check_plain(_node_by_term_image_field_as_link_content_type_helper_get_entity_title($entity_type, $entity));
  $content = entity_view($entity_type, array($entity), $view_mode, $GLOBALS['language']->language);

  $block->title = $entity_title;
  $block->content = $content;

  return $block;
}

/**
 * Returns the administrative title for a node.
 */
function entity_rendered_view_mode_content_type_admin_title($subtype, $conf) {
  $values = $conf['values'];
  $entity_type = $values['entity'];
  $bundle_type = $values['bundle'];
  $eid = entity_autocomplete_get_id($values['eid']);
  $entities_loaded = entity_load($entity_type, array($eid));
  $entity = $entities_loaded[$eid];
  $entity_type_label = (isset($entity->type)) ? $entity->type : $bundle_type;

  $title_raw = _node_by_term_image_field_as_link_content_type_helper_get_entity_title($entity_type, $entity);

  $title = check_plain($title_raw);

  return t('[@entity_type ID-@eid]@title ~ Rendered Entity', array(
    '@eid' => $eid,
    '@title' => ' ' . $title,
    '@entity_type' => ucwords($entity_type_label),
  ));
}

/**
 * Display the administrative information for a node pane.
 */
function entity_rendered_view_mode_content_type_admin_info($subtype, $conf) {
  $values = $conf['values'];

  $items = array();

  foreach ($values as $var_name => $var_value) {
    $items[] = "<strong>\${$var_name}:</strong> {$var_value}";
  }

  $block = new stdClass();

  $block->title = t('Rendered Entity: Settings');

  $block->content = theme_item_list(array(
    'items' => $items,
    'title' => t('Settings'),
    'type' => 'ul',
    'attributes' => array(),
  ));

  return $block;
}

/**
 * Helper to discern from where form values should be pulled for form building.
 */
function _entity_rendered_view_mode_content_type_get_edit_form_values(&$form_state) {
  // Var for keeping track of which use case this func was called in.
  $trigger_state = '';

  // Check to see at what point this func was called - could be one of three or four cases
  // 1) form requested for the very first time - meaning that we need fresh default values.
  if (!isset($form_state['input']['_triggering_element_name']) && !$form_state['submitted']) {
    $trigger_state = 'defaults';
  }
  // 2) form requested by an ajax call, this means values will need to be updated from $form_state['input'].
  elseif (isset($form_state['input']['_triggering_element_name'])) {
    $trigger_state = 'input';
  }
  // 3) form requested onSubmit or after it was submitted and values were
  // saved - values need to be pulled from $form_state['values'].
  else {
    $trigger_state = 'values';
  }

  // get/set values appropriate to triggering use case and return them.
  return _entity_rendered_view_mode_content_type_set_edit_form_values($trigger_state, $form_state);
}

/**
 * Helper to actually get the values that will be used as #default_value for form fields.
 */
function _entity_rendered_view_mode_content_type_set_edit_form_values($form_state_key_to_use, $form_state) {
  // Values to be used as #default_value.
  $values = array();

  // If $form_state_key_to_use == 'defaults', it means that this is the initial form build.
  if ($form_state_key_to_use == 'defaults' || $form_state_key_to_use == 'input') {
    // Pull out $conf for cleanliness.
    $conf = $form_state['conf'];

    // get-set Entity.
    if ($form_state_key_to_use == 'defaults') {
      reset($conf['field_settings']['entity_list']);
      $values['entity'] = $entity = key($conf['field_settings']['entity_list']);
    }
    else {
      $values['entity'] = $entity = $form_state[$form_state_key_to_use]['entity'];
    }

    // get-set Bundle.
    if ($form_state_key_to_use == 'defaults') {
      list(, $bundle) = _entity_rendered_view_mode_content_type_helper_get_bundle_list($entity);
      $values['bundle'] = $bundle;
    }
    else {
      $values['bundle'] = $bundle = $form_state[$form_state_key_to_use]['entity_subfields']['bundle'];
    }

    // get-set View Mode.
    $values['view_mode'] = '';
    $entity_info = entity_get_info($entity);
    foreach (array_keys($entity_info['view modes']) as $key) {
      // Ensure that the current view_mode is not in the to-ignore list and if so, set $values['view_mode'].
      if (!in_array($key, $conf['field_settings']['ignored_view_modes'])) {
        $values['view_mode'] = $key;
      }
      // If $values['view_mode'] has received a value, break the loop.
      if (strlen($values['view_mode'])) {
        break;
      }
    }

    // Set EID.
    $values['eid'] = '';

    return $values;
  }
  else {
    $input = $form_state[$form_state_key_to_use];
    $entity = $values['entity'] = $input['entity'];
    $bundle = $values['bundle'] = $input['entity_subfields']['bundle'];
    $values['view_mode'] = $input['entity_subfields']['view_mode'];
    $values['eid'] = $input['entity_subfields']['eid'];

    return $values;
  }
}

/**
 * Helper to generate select widget of input Entity's Bundles.
 */
function _entity_rendered_view_mode_content_type_get_entity_field($entity, $values, $form_state) {
  $conf = $form_state['conf'];

  reset($conf['field_settings']['entity_list']);
  $default_value = (isset($values['entity'])) ? $values['entity'] : key($conf['field_settings']['entity_list']);

  return array(
    '#title' => t('Select Entity Type'),
    '#type' => 'select',
    '#options' => $conf['field_settings']['entity_list'],
    '#default_value' => $default_value,
    '#weight' => 20,
    '#ajax' => array(
      'path' => 'ctools_aep/%ctools_js/entity_rendered_view_mode',
      'wrapper' => 'entity_subfields',
      'effect' => 'fade',
    ),
  );
}

/**
 * Helper to generate select widget of input Entity's Bundles.
 */
function _entity_rendered_view_mode_content_type_get_bundle_field($entity, $values, $form_state) {
  // Generate a keyed array of bundle names.
  list($bundles, $first_bundle_name) = _entity_rendered_view_mode_content_type_helper_get_bundle_list($entity);

  // Set the default value depending upon availability of the same from $defaults.
  $bundle = ((isset($values['bundle']) && strlen($values['bundle'])) && (isset($values['entity']) && ($entity == $values['entity']))) ? $values['bundle'] : $first_bundle_name;

  $trace = debug_backtrace();
  $caller = array_shift($trace);

  return array(
    '#title' => t(ucwords($entity) . ' Type'),
    '#type' => 'select',
    '#id' => "{$entity}_bundles",
    '#options' => $bundles,
    '#default_value' => $bundle,
    '#weight' => 22,
    '#ajax' => array(
      'path' => 'ctools_aep/%ctools_js/entity_rendered_view_mode',
      'wrapper' => 'eid_field',
      'effect' => 'fade',
    ),
  );
}

/**
 * Helper to generate select widget of input Entity's View Modes.
 */
function _entity_rendered_view_mode_content_type_get_view_mode_field($entity, $values, $form_state) {
  $conf = $form_state['conf'];
  $def_view_mode = (isset($values['view_mode']) && strlen($values['view_mode'])) ? $values['view_mode'] : 'full';

  $view_modes = array();
  $entity_info = entity_get_info($entity);
  foreach (array_keys($entity_info['view modes']) as $key) {
    if (!in_array($key, $conf['field_settings']['ignored_view_modes'])) {
      $view_modes[$key] = $entity_info['view modes'][$key]['label'];
    }
  }

  return array(
    '#title' => t("View Mode"),
    '#type' => 'select',
    '#description' => t('Select the view mode that should be used to render this Entity Bundle.'),
    '#options' => $view_modes,
    '#default_value' => $def_view_mode,
    '#weight' => 23,
  );
}

/**
 * Helper to generate autocomplete widget for retrieving an ID based upon Entity and Bundle types.
 */
function _entity_rendered_view_mode_content_type_get_eid_field($entity, $values, $form_state) {
  // Bundle by current entity check - necessary when the form has been submitted and then reopened.
  list($cur_entity_bundle_list, $cur_entity_bundle) = _entity_rendered_view_mode_content_type_helper_get_bundle_list($entity);

  // Set $bundle if none was provided.
  if (isset($values['bundle']) && !empty($values['bundle']) && in_array($values['bundle'], array_keys($cur_entity_bundle_list))) {
    $bundle = $values['bundle'];
  }
  else {
    $bundle = $cur_entity_bundle;
  }

  // Clean up entity and bundle names for use in form element label.
  $ent_name = ucwords($entity);
  $bun_name = ucwords($bundle);

  // Get default value of eid.
  $default_eid = (isset($values['eid']) && strlen($values['eid']) && isset($values['bundle']) && $values['bundle'] == $bundle) ? $values['eid'] : '';

  // ref: http://drupal.org/project/entity_autocomplete
  return array(
    '#title' => t("Enter the title or ID of a {$bun_name} {$ent_name}."),
    '#type' => 'textfield',
    '#id' => "{$entity}_{$bundle}_eid_input",
    '#default_value' => $default_eid,
    '#maxlength' => 512,
    '#autocomplete_path' => "entity-autocomplete/bundle/{$entity}/{$bundle}",
    '#weight' => 24,
    '#prefix' => "<div id='eid_field'>",
    '#suffix' => '</div>',
  );
}

/**
 * Helper to generate select widget of input Entity's Bundles.
 */
function _entity_rendered_view_mode_content_type_helper_get_bundle_list($entity) {
  // Get the list of bundles for this Entity.
  $bundles_info = field_info_bundles($entity);

  // Holder for select element's options.
  $bundles = array();

  // Build an array for use as the select element's options.
  foreach ($bundles_info as $key => $val) {
    $bundles[$key] = $val['label'];
  }

  // Holder for the first bundle name to use as default value if $defaults['bundle'] is empty.
  reset($bundles);
  $first_bundle_name = (string) key($bundles);

  return array($bundles, $first_bundle_name);
}

/**
 *
 */
function _node_by_term_image_field_as_link_content_type_helper_get_entity_title($entity_type, $entity) {
  // Get the title, where to get this varies according to $entity_type.
  switch ($entity_type) {
    // ============================================================.
    case 'node':
      $title_raw = $entity->title;
      break;

    // ============================================================.
    case 'file':
      $title_raw = $entity->filename;
      break;

    // ============================================================.
    case 'user':
      $title_raw = $entity->name;
      break;

    // ============================================================.
    default:
      $title_raw = '';
      break;
  }

  return $title_raw;
}

/**
 * A helpful note on states for this form.
 *
 * When the form is first opened by choosing the content type for a new Pane, the following is true:
 *    !isset($form_state['input']['_triggering_element_name']) && !$form_state['submitted']
 *
 * When an ajax call has been made to change some part of the form, the following will be true:
 *    isset($form_state['input']['_triggering_element_name'])
 *
 * Finally, in the last use-case scenario, when the form has been submitted and
 * is re-opened from the Pane management page, the following will hold true:
 *    isset($form_state['values']) && isset($form_state['conf']['values])
 *        - please note that $form_state['conf']['values] is set in
 *            entity_rendered_view_mode_content_type_edit_form_submit()
 *          and then removed again at the beginning of
 *            entity_rendered_view_mode_content_type_edit_form()
 */
