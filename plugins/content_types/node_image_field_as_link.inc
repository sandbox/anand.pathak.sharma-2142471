<?php

/**
 * @file =============================================================================
 * TODO!!!
 * todo: form validation
 * todo: content type render function
 * ========================================================================== .*/

/* =============================================================================
    Ref.: http://drupal.stackexchange.com/questions/33336/how-does-one-create-a-new-ctools-plugin-content-type-access-etc
   ========================================================================== */
$plugin = array(
  'title' => t('Node as Image Link'),
  'description' => t('Output a selected Node as an image (based upon an image field attached to the Node Type) that will be optionally wrapped in a link.'),
  'category' => t('ADAPL'),
  'admin title' => 'node_image_field_as_link_content_type_admin_title',
  'admin info' => 'node_image_field_as_link_content_type_admin_info',
  'single' => TRUE,
  'icon' => 'icon_node.png',
  'js' => array('misc/autocomplete.js'),
  'edit form' => array(
    'node_image_field_as_link_content_type_edit_form' => t('Select Entity Options'),
  ),
  'render callback' => 'node_image_field_as_link_content_type_render',
  'top level' => TRUE,
  'defaults' => array(
    'field_settings' => array(
      'entity_list' => array('node' => 'Node'),
      // Used for magic loading of get_FIELD_NAME functions that return renderable array of field.
      'settings_field_list' => array(
        'entity',
        'bundle',
        'image_field',
        'image_style',
        'nid',
        'wrap_in_link',
      ),
      // Used in _node_image_field_as_link_content_type_set_edit_form_values()
      'wrap_in_link_fields' => array(
        'yes_or_no',
        'node_or_custom',
        'custom_url',
      ),
      // Used in _node_image_field_as_link_content_type_helper_get_bundle_list()
      'ignored_entity_bundles' => array('panel', 'webform'),
      // Used in _node_image_field_as_link_content_type_helper_get_image_style_list()
      'default_image_style' => 'square_thumbnail',
    ),
  ),
);

/**
 * The form to add or edit a node as content.
 */
function node_image_field_as_link_content_type_edit_form($form, &$form_state) {
  // Get conf.
  $conf = $form_state['conf'];
  // Get values.
  if (isset($form_state['conf']['values'])) {
    $values = $form_state['conf']['values'];
    unset($form_state['conf']['values']);
  }
  else {
    $values = _node_image_field_as_link_content_type_get_edit_form_values($form_state);
  }

  $form['#tree'] = TRUE;

  $form['bundle_subfields'] = array(
    '#type' => 'fieldset',
    '#title' => 'Node Settings',
    '#prefix' => '<div id="bundle_subfields">',
    '#suffix' => '</div>',
    '#weight' => 51,
  );

  // Field generating funcs follow a set naming convention - "_node_image_field_as_link_content_type_get_{$field_name}_field"
  // confirm that the func exists and then call it to add it to the $form['settings'] fieldset.
  foreach ($conf['field_settings']['settings_field_list'] as $field_name) {
    $bundle_subfields = array('nid', 'image_field');
    $func_name = "_node_image_field_as_link_content_type_get_{$field_name}_field";
    if (is_callable($func_name)) {
      // Check to see if the current field needs to be added as a child of $form['bundle_subfields'].
      if (in_array($field_name, $bundle_subfields)) {
        $form['bundle_subfields'][$field_name] = call_user_func($func_name, $values['entity'], $values, $form_state);
      }
      else {
        $form[$field_name] = call_user_func($func_name, $values['entity'], $values, $form_state);
      }
    }
  }

  // Remove undesired ctools native content type form elements.
  $unwanted_fields = array(
    'override_title',
    'override_title_text',
    'override_title_markup',
    'aligner_start',
    'aligner_stop',
  );
  foreach ($unwanted_fields as $field_name) {
    unset($form[$field_name]);
  }

  return $form;
}

/**
 * Ajax callback handler.
 */
function node_image_field_as_link_content_type_edit_form_ajax_callback($js = NULL) {
  // Include ctools libraries.
  ctools_include('ajax');

  // Pulls form from cache and updates $form_state with input from $_POST.
  list($form, $form_state, $form_id, $form_build_id) = ajax_get_form();

  // Rebuilds the form.
  drupal_process_form($form_id, $form, $form_state);

  // Initiate response.
  ajax_deliver(drupal_render($form['bundle_subfields']));
}

/**
 * Pull values from $form_state['values'] OR $form_state['input'] into $form_state['conf'].
 */
function node_image_field_as_link_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['values'] = _node_image_field_as_link_content_type_get_edit_form_values($form_state);
}

/**
 * Ensure that an Image or Media field has been selected.
 */
function node_image_field_as_link_content_type_edit_form_validate($form, &$form_state) {
  // Convenience assignments.
  $val = $form_state['values'];
  $entity = $val['entity'];
  $bundle = $val['bundle'];
  $bundles_info = field_info_instances($entity, $bundle);
  $nid = entity_autocomplete_get_id($val['bundle_subfields']['nid']);
  $image_field = $val['bundle_subfields']['image_field'];
  $image_style = $val['image_style'];
  $link_yes_or_no = $val['wrap_in_link']['yes_or_no'];
  $link_node_or_custom = $val['wrap_in_link']['node_or_custom'];
  $link_custom_url = $val['wrap_in_link']['custom_url'];

  // Ensure the NID is retrievable.
  $node = node_load($nid);
  if (!$node) {
    form_set_error('bundle_subfields][nid', t('There was a problem with the Node ID specified. The selected Node does not exist.'));
  }
  // Ensure the Node is of the correct bundle type.
  elseif ($node->type !== $bundle) {
    form_set_error('bundle_subfields][nid', t('There was a problem with the Node ID specified. The Node was not of the selected Node Type.'));
  }
  // Make sure the Image/Media field has a value.
  elseif (!isset($node->{$image_field}) || empty($node->{$image_field})) {
    $field_label = $bundles_info[$image_field]['label'];
    form_set_error('bundle_subfields', t("The node you have selected does not have a value for the specified Image/Media field. Please correct this by doing one of the following: <ul><li style='list-style:disc outside none'>Update the selected Node with a value for the Image/Media field ({$field_label}).</li><li style='list-style:disc outside none'>Choose a different Image/Media field to use from the selected Node.</li><li style='list-style:disc outside none'>Select another Node.</li></ul>"));
  }

  // Validate Bundle / Node Type.
  list($bundle_list) = _node_image_field_as_link_content_type_helper_get_bundle_list($entity, $form_state);
  if (!in_array($bundle, array_keys($bundle_list))) {
    form_set_error('bundle', t('The selected Node Type does not exist.'));
  }

  // Validate Image / Media field.
  $file = file_load($node->{$image_field}[$node->language][0]['fid']);
  if (!preg_match('@^image@', $file->filemime)) {
    form_set_error('image_style', t('The file value of this field does not appear to be an Image file. Please edit the value of this field on the selected Node or choose another Node/image field.'));
  }

  // Validate Image Style.
  $style_info = image_style_load($image_style);
  if (!$style_info || empty($style_info)) {
    form_set_error('image_style', t('There was a problem with the selected Image Style, please try again.'));
  }

  // Validate Link settings.
  if ((bool) $link_yes_or_no) {
    if ($link_node_or_custom !== 'node' && $link_node_or_custom !== 'custom') {
      form_set_error('wrap_in_link', t('Please select either "Link to Node" or "Specify custom URL".'));
    }
    elseif ($link_node_or_custom === 'custom' && !strlen($link_custom_url)) {
      form_set_error('wrap_in_link][custom_url', t('Please enter a value for "Custom URL".'));
    }
    elseif ($link_node_or_custom === 'custom' && strlen($link_custom_url)) {
      if (!url_is_external($link_custom_url) && !drupal_valid_path($link_custom_url, TRUE)) {
        form_set_error('wrap_in_link][custom_url', t('The internal URL specified does not exist. Please check and submit again.'));
      }
      elseif (url_is_external($link_custom_url) && !valid_url($link_custom_url, TRUE)) {
        form_set_error('wrap_in_link][custom_url', t('The URL specified does not seem to be correct. Please check and submit again.'));
      }
    }
  }
}

/**
 * Render Entity options chosen by user.
 */
function node_image_field_as_link_content_type_render($subtype, $conf, $panel_args) {
  // Pull out values from configuration.
  $values = $conf['values'];

  // Get configuration values.
  $nid = entity_autocomplete_get_id($values['nid']);
  $image_field = $values['image_field'];
  $image_style = $values['image_style'];
  $link_to_node = (bool) $values['wrap_in_link_yes_or_no'];
  $node_or_custom = $values['wrap_in_link_node_or_custom'];
  $custom_url = $values['wrap_in_link_custom_url'];

  $block = new stdClass();

  // Support node translation.
  if (module_exists('translation')) {
    if ($translations = module_invoke('translation', 'node_get_translations', $nid)) {
      if (isset($translations[$GLOBALS['language']->language])) {
        $nid = $translations[$GLOBALS['language']->language]->nid;
      }
    }
  }

  if (!is_numeric($nid)) {
    return FALSE;
  }

  $node = node_load($nid);
  if (!node_access('view', $node)) {
    return FALSE;
  }

  // Don't store viewed node data on the node, this can mess up other views of the node.
  $node = clone($node);
  $block->delta = $node->nid;
  $node_title = check_plain($node->title);
  $content = '';

  // Pull the image file's FID and load the file.
  $file = file_load($node->{$image_field}[$node->language][0]['fid']);
  $image_info = image_get_info(image_style_path($image_style, $file->uri));
  $image_url = image_style_url($image_style, $file->uri);
  $image = theme_image(array(
    'path' => $image_url,
    'title' => $node_title,
    'alt' => '',
    'attributes' => array(),
  ));
  // Commented out below-version of $image as it inserts width/height attributes that I don't want
  // Created new version of $image above to use instead
  /*$image = theme_image_style(array(
  'style_name' => $image_style,
  'path' => $file->uri,
  'title' => $node_title,
  'alt' => '',
  'width' => $image_info['width'],
  'height' => $image_info['height'],
  ));*/

  // Get the path to the node or pull up the custom url.
  if ($link_to_node) {
    if ($node_or_custom === 'node') {
      $link_path = drupal_get_path_alias("node/{$node->nid}", $GLOBALS['language']->language);
    }
    elseif ($node_or_custom === 'custom') {
      $link_path = check_plain($custom_url);
    }
    else {
      return FALSE;
    }

    $content = l($image, $link_path, array(
      'html' => TRUE,
    ));
  }
  else {
    $content = $image;
  }

  $block->content = $content;

  return $block;
}

/**
 * Returns the administrative title for a node.
 */
function node_image_field_as_link_content_type_admin_title($subtype, $conf) {
  $values = $conf['values'];
  $entity_type = $values['entity'];
  $bundle_type = $values['bundle'];
  $nid = entity_autocomplete_get_id($values['nid']);
  $entity = array_pop(entity_load($entity_type, array($nid)));
  $title = check_plain($entity->title);

  return t('[@bundle_type ID-@nid] @title | Node Image as Link', array(
    '@nid' => $nid,
    '@title' => $title,
    '@bundle_type' => ucwords($bundle_type),
  ));
}

/**
 * Display the administrative information for a node pane.
 */
function node_image_field_as_link_content_type_admin_info($subtype, $conf) {
  $values = $conf['values'];

  $items = array();

  foreach ($values as $var_name => $var_value) {
    $items[] = "<strong>\${$var_name}:</strong> {$var_value}";
  }

  $block = new stdClass();

  $block->title = t('Node as Image Link: Settings');

  $block->content = theme_item_list(array(
    'items' => $items,
    'title' => t('Settings'),
    'type' => 'ul',
    'attributes' => array(),
  ));

  return $block;
}

/**
 * Helper to discern from where form values should be pulled for form building.
 */
function _node_image_field_as_link_content_type_get_edit_form_values(&$form_state) {
  $values = NULL;

  if (!isset($form_state['input']['_triggering_element_name']) && !$form_state['submitted']) {
    $values = _node_image_field_as_link_content_type_set_edit_form_values('defaults', $form_state);
  }
  elseif (isset($form_state['input']['_triggering_element_name'])) {
    $values = _node_image_field_as_link_content_type_set_edit_form_values('input', $form_state);
  }
  else {
    $values = _node_image_field_as_link_content_type_set_edit_form_values('values', $form_state);
  }

  return $values;
}

/**
 * Helper to actually get the values that will be used as #default_value for form fields.
 */
function _node_image_field_as_link_content_type_set_edit_form_values($form_state_key_to_use, $form_state) {
  // Values to be used as #default_value.
  $values = array();

  // If $form_state_key_to_use == 'defaults', it means that this is the initial form build.
  if ($form_state_key_to_use == 'defaults' || $form_state_key_to_use == 'input') {
    // Pull out $conf for cleanliness.
    $conf = $form_state['conf'];

    // get-set Entity.
    if ($form_state_key_to_use == 'defaults') {
      reset($conf['field_settings']['entity_list']);
      $values['entity'] = $entity = key($conf['field_settings']['entity_list']);
    }
    else {
      $values['entity'] = $entity = $form_state[$form_state_key_to_use]['entity'];
    }

    // get-set Bundle.
    if ($form_state_key_to_use == 'defaults') {
      list(, $bundle) = _node_image_field_as_link_content_type_helper_get_bundle_list($entity, $form_state);
      $values['bundle'] = $bundle;
    }
    else {
      $values['bundle'] = $bundle = $form_state[$form_state_key_to_use]['bundle'];
    }

    // Set nid.
    $values['nid'] = '';

    // Set image field values.
    list(, $image_field) = _node_image_field_as_link_content_type_helper_get_image_field_list($entity, $values);
    $values['image_field'] = $image_field;

    // Set image styles values.
    list(, $image_style) = _node_image_field_as_link_content_type_helper_get_image_style_list(array(), $form_state);
    $values['image_style'] = $image_style;

    // Set link fieldset values 'wrap_link_yes_or_no'.
    $values['wrap_in_link_yes_or_no'] = 0;
    $values['wrap_in_link_node_or_custom'] = 'node';
    $values['wrap_in_link_custom_url'] = '';

    return $values;
  }
  else {
    // If the form was pulled via Ajax or was submitted ("Finish" button was used) then $form_state['values'] need to be used to set $form_state['conf']
    // please note that these values are separately accessed directly from $form_state['values] in _node_image_field_as_link_content_type_edit_form_validate()
    $input = $form_state[$form_state_key_to_use];
    $entity = $values['entity'] = $input['entity'];
    $bundle = $values['bundle'] = $input['bundle'];
    $values['nid'] = $input['bundle_subfields']['nid'];
    $values['image_field'] = $input['bundle_subfields']['image_field'];
    $values['image_style'] = $input['image_style'];

    foreach ($form_state['conf']['field_settings']['wrap_in_link_fields'] as $field_key) {
      $values["wrap_in_link_{$field_key}"] = $input['wrap_in_link'][$field_key];
    }

    return $values;
  }
}

/**
 * Helper to generate select widget of input Entity's Bundles.
 */
function _node_image_field_as_link_content_type_get_entity_field($entity, $values, $form_state) {
  reset($form_state['conf']['field_settings']['entity_list']);
  $default_value = key($form_state['conf']['field_settings']['entity_list']);

  return array(
    '#title' => t('Select Entity Type'),
    '#type' => 'hidden',
    '#default_value' => $default_value,
  );
}

/**
 * Helper to generate select widget of input Entity's Bundles.
 */
function _node_image_field_as_link_content_type_get_bundle_field($entity, $values, $form_state) {
  // Generate a keyed array of bundle names.
  list($bundles, $default_bundle) = _node_image_field_as_link_content_type_helper_get_bundle_list($entity, $form_state);

  // Set the default value depending upon availability of the same from $defaults.
  $default_value = ((isset($values['bundle']) && strlen($values['bundle'])) && (isset($values['entity']) && ($entity == $values['entity']))) ? $values['bundle'] : $default_bundle;

  $trace = debug_backtrace();
  $caller = array_shift($trace);

  return array(
    '#title' => t(ucwords($entity) . ' Type'),
    '#type' => 'select',
    '#id' => "{$entity}_bundles",
    '#options' => $bundles,
    '#default_value' => $default_value,
    '#weight' => 50,
    '#ajax' => array(
      'path' => 'ctools_aep/%ctools_js/node_image_field_as_link',
      'wrapper' => 'bundle_subfields',
      'effect' => 'fade',
    ),
  );
}

/**
 * Helper to generate autocomplete widget for retrieving an ID based upon Entity and Bundle types.
 */
function _node_image_field_as_link_content_type_get_nid_field($entity, $values, $form_state) {
  // Set $bundle if none was provided.
  if (!isset($values['bundle'])) {
    list(, $bundle) = _node_image_field_as_link_content_type_helper_get_bundle_list($entity, $form_state);
  }
  else {
    $bundle = $values['bundle'];
  }

  // Clean up entity and bundle names for use in form element label.
  $ent_name = ucwords($entity);
  $bun_name = ucwords($bundle);

  // Get default value of nid.
  $default_nid = (isset($values['nid']) && strlen($values['nid']) && isset($values['bundle']) && $values['bundle'] == $bundle) ? $values['nid'] : '';

  // ref: http://drupal.org/project/entity_autocomplete
  return array(
    '#tree' => TRUE,
    '#title' => t("Enter the title or ID of a {$bun_name} {$ent_name}."),
    '#type' => 'textfield',
    '#id' => "{$bundle}_nid_input",
    '#default_value' => $default_nid,
    '#maxlength' => 512,
    '#autocomplete_path' => "entity-autocomplete/bundle/{$entity}/{$bundle}",
  );
}

/**
 * Helper to generate select widget of input Entity's selected Bundle's Image and Media fields.
 */
function _node_image_field_as_link_content_type_get_image_field_field($entity, $values, $form_state) {
  // Holder for actual field info.
  list($image_fields, $default_value) = _node_image_field_as_link_content_type_helper_get_image_field_list($entity, $values);

  if (!empty($image_fields)) {
    return array(
      '#type' => 'select',
      '#title' => t("Image or Media Field"),
      '#description' => t("Please select the Image or Media field on the selected Entity Bundle that will be used.</br><strong>NOTE:</strong> If the field is a multiple value field, the value with the lightest weight will be used."),
      '#options' => $image_fields,
      '#default_value' => $default_value,
      '#weight' => 60,
    );
  }
  else {
    return array(
      '#title' => t("Node Type has no Image or Media fields."),
      '#markup' => t("<strong style='color:f00'>Please select a different Node Type as the selected one does has neither an Image nor a Media field.</strong>"),
      '#weight' => 61,
      '#prefix' => '<div id="image_field" class="form-item form-type-select form-item-bundle">',
      '#suffix' => '</div>',
    );
  }
}

/**
 * Helper to generate select widget of input Entity's View Modes.
 */
function _node_image_field_as_link_content_type_get_image_style_field($entity, $values, $form_state) {
  // Get a list of image_style keys.
  list($image_styles, $default_value) = _node_image_field_as_link_content_type_helper_get_image_style_list($values, $form_state);

  return array(
    '#title' => t("Image Style"),
    '#type' => 'select',
    '#description' => t('Select the image style that should be used to render this Image/Media-Image field.'),
    '#options' => $image_styles,
    '#default_value' => $default_value,
    '#weight' => 53,
  );
}

/**
 * Helper to generate fieldset for the link-wrapper settings.
 */
function _node_image_field_as_link_content_type_get_wrap_in_link_field($entity, $values, $form_state) {
  $fieldset = array(
    '#type' => 'fieldset',
    '#title' => t('Link Settings'),
    '#weight' => 54,
  );
  $fieldset['yes_or_no'] = array(
    '#type' => 'checkbox',
    '#title' => t('Wrap Node-Image in Link'),
    '#description' => t('Please check if you wish for the output Image to be wrapped in a link.'),
    '#default_value' => isset($values['wrap_in_link_yes_or_no']) ? $values['wrap_in_link_yes_or_no'] : 0,
  );
  $fieldset['node_or_custom'] = array(
    '#type' => 'radios',
    '#title' => t('Link to Node or Custom'),
    '#description' => t('Please check if you wish for the link.'),
    '#default_value' => isset($values['wrap_in_link_node_or_custom']) ? $values['wrap_in_link_node_or_custom'] : 'node',
    '#options' => array(
      'node' => 'Link to Node',
      'custom' => 'Specify custom URL',
    ),
    '#states' => array(
      'visible' => array(
        'input[name="wrap_in_link[yes_or_no]"]' => array('checked' => TRUE),
      ),
      'disabled' => array(
        'input[name="wrap_in_link[yes_or_no]"]' => array('checked' => FALSE),
      ),
    ),
  );
  $fieldset['custom_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom URL'),
    '#description' => t('Please specify the custom URL you wish to link to.'),
    '#default_value' => isset($values['wrap_in_link_custom_url']) ? $values['wrap_in_link_custom_url'] : '',
    '#states' => array(
      'visible' => array(
        'input[type="checkbox"][name*="yes_or_no"]' => array('checked' => TRUE),
        'input[type="radio"][name*="node_or_custom"]' => array('value' => 'custom'),
      ),
      'disabled' => array(
        'input[type="checkbox"][name*="yes_or_no"]' => array('checked' => FALSE),
        'input[type="radio"][name*="node_or_custom"]' => array('value' => 'node'),
      ),
    ),
  );

  return $fieldset;
}

/**
 * Helper to generate select widget of input Entity's Bundles.
 */
function _node_image_field_as_link_content_type_helper_get_bundle_list($entity, $form_state) {
  // Get the list of bundles for this Entity.
  $bundles_info = field_info_bundles($entity);

  // Ignored bundles.
  $ignored_bundles = $form_state['conf']['field_settings']['ignored_entity_bundles'];

  // Holder for select element's options.
  $bundles = array();

  // Build an array for use as the select element's options.
  foreach ($bundles_info as $key => $val) {
    // Skip over bundles that cannot be used - see $plugins for list of ignored bundles.
    if (in_array($key, $ignored_bundles)) {
      continue;
    }
    $bundles[$key] = $val['label'];
  }

  // Holder for the first bundle name to use as default value if $defaults['bundle'] is empty.
  reset($bundles);
  $default_bundle = (string) key($bundles);

  return array($bundles, $default_bundle);
}

/**
 * Helper to get the default image style - will be the first key in array returned by image_styles()
 */
function _node_image_field_as_link_content_type_helper_get_image_field_list($entity, $values) {
  // Holder for the list of image/media fields.
  $image_fields = array();

  // Get field info for this bundle type.
  $bundle_field_info = field_info_instances($entity, $values['bundle']);

  foreach ($bundle_field_info as $field_name => $bundle_field_settings) {
    // Load field info.
    $field_info = field_info_field($field_name);

    // Check type of field.
    if ($field_info['type'] == 'media' || $field_info['type'] == 'image') {
      $image_fields[$field_name] = $bundle_field_settings['label'];
    }
  }

  reset($image_fields);
  $default_image_field = (isset($values['image_field']) && in_array($values['image_field'], array_keys($image_fields))) ? $values['image_field'] : key($image_fields);

  return array($image_fields, $default_image_field);
}

/**
 * Helper to get the default image style - will be the first key in array returned by image_styles()
 */
function _node_image_field_as_link_content_type_helper_get_image_style_list($values, $form_state) {
  $conf = $form_state['conf'];
  $image_styles = array_keys(image_styles());

  // Format and cleanup into an assoc. array for use as select options.
  foreach ($image_styles as $cur_index => $style_key_name) {
    unset($image_styles[$cur_index]);
    $image_styles[$style_key_name] = ucwords(preg_replace('@_@', ' ', $style_key_name));
  }

  reset($image_styles);
  $default_image_style = isset($values['image_style']) ? $values['image_style'] : $conf['field_settings']['default_image_style'];

  return array($image_styles, $default_image_style);
}
